# Homework from 04/05/2020 and 05/05/2020

This is project with homework for Akademia Python 2019/2020

## Table of contents
* Description of files
* Technologies
* Setup
* Author of project

## Description of files
A short description of the files that the project contains

* _taks_1.py_ -> this files contains first task (homework from 04/05/2020) - variables
* _taks_2.py_ -> this files contains first task (homework from 05/05/2020) - classes

## Technologies
Project is created with:
* Python 3

## Setup

* run _taks_1.py_ to start homework task no. 1 (variables)
* run _taks_2.py_ to start homework task no. 2 (classes)

## Author of project
@Tirpitz_Player aka Mikołaj Błażejewski
