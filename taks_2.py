#!/usr/bin/enc python3
""" Homework from 05/05/2020 created by @Mikołaj Błażejewski """
 
 
class Plant:
    """ Plant class """
    pass
 
 
class Perfume:
    """ Perfume class """
    pass
 
 
class Bread:
    """ Bread class """
    pass
 
 
# Create objects from classes
plant = Plant()
perfume = Perfume()
bread = Bread()
 
# Print information about declared objects
print(plant)
print(perfume)
print(bread)
