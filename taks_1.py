#!/usr/bin/enc python3
""" Homework from 04/05/2020 created by @Mikołaj Błażejewski """

# Declaration of variable
user_name = str("Mikołaj Błażejewski")  # Variable holding author name
user_age = int(21)  # Variable holding author age
user_height = float(1.81)  # Variable holding author height in meters

# Print information from variables on terminal
print("Hi " + user_name + "! You age is " + str(user_age) + " and you are " + str(user_height) + " meters tall")
